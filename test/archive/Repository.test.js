import expectEvent from "zeppelin-solidity/test/helpers/expectEvent";
import assertRequire from '../helpers/assertRequire';
import assertRevert from "zeppelin-solidity/test/helpers/assertRevert";

var expect = require('chai').expect;
var should = require('chai').should();

var Repository = artifacts.require("./archive/Repository.sol");

contract('Repository', function(accounts) {
    let repository;

    const orbitdbUris = [
        '/orbitdb/QmWuXFFKkTA477sb7DRRojZm83YvoWYG5hWNVVabrA7LXS/metadata'
    ];

    const assetUris = [
        'ipfs://Qmb4atcgbbN5v4CDJ8nz5QG5L2pgwSTLd3raDrnyhLjnUH',
        'http://www.example.com/asset.pdf',
        'ftp://ftp.example.com/asset.ogv',
    ];

    beforeEach(async () => {
        repository = await Repository.new("KnowledgeArc.Network", "KNOWLEDGEARC");
    });

    it("sets name and symbol", async () => {
        var symbol = await repository.symbol();
        var name = await repository.name();

        symbol.should.equal('KNOWLEDGEARC');
        name.should.equal('KnowledgeArc.Network');
    });

    it("archives an asset and its metadata", async () => {
        await expectEvent.inTransaction(
            repository.archive(assetUris[0], orbitdbUris[0]),
            'AssetArchived'
        );
    });

    it("archives an asset and its metadata and mints a token", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0]);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = Number(log.args.tokenId);
                tokenId.should.equal(1);

                break;
            }
        }
    });

    it("archives an asset and its metadata and gets the token URI back", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0]);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = Number(log.args.tokenId);
                var tokenURI = await repository.tokenURI(tokenId);
                tokenURI.should.equal(orbitdbUris[0]);
                break;
            }
        }
    });

    it("gets token for a particular owner at a particular index", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var tokenId = await repository.tokenOfOwnerByIndex(accounts[0], 0);

        tokenId.toNumber().should.equal(1);
    });

    it("gets the total supply of tokens", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var supply = await repository.totalSupply();

        supply.toNumber().should.equal(1);
    });

    it("gets the token by index", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var tokenId = await repository.tokenByIndex(0);

        tokenId.toNumber().should.equal(1);
    });

    it("gets the token balance of an account", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var balance = await repository.balanceOf(accounts[0]);

        balance.toNumber().should.equal(1);
    });

    it("gets the owner of the token", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var owner = await repository.ownerOf(1);

        owner.should.equal(accounts[0]);
    });

    it("checks if the token exists", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var exists = await repository.exists(1);

        exists.should.be.true;
    });

    it("checks if the token doesn't exist", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        var exists = await repository.exists(0);

        exists.should.be.false;
    });

    it("approves another address to transfer a token", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        const result = await repository.approve(accounts[1], 1);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            log.event.should.equal("Approval");
        }
    });

    it("transfers a token from token owner to another address", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        const result = await repository.safeTransferFrom(accounts[0], accounts[1], 1);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            log.event.should.equal("Transfer");
        }
    });

    it("attempts to transfer a non-existent token", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        await assertRevert(repository.safeTransferFrom(accounts[0], accounts[1], 2));
    });

    it("attempts to transfer a token from an address without permission", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        await assertRevert(repository.safeTransferFrom(accounts[0], accounts[1], 1, {from: accounts[2]}));
    });

    it("transfers a token on the owner's behalf", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        await repository.approve(accounts[2], 1);

        const result = await repository.safeTransferFrom(accounts[0], accounts[1], 1, {from: accounts[2]});

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            log.event.should.equal("Transfer");
        }
    });

    it("transfers all tokens on the owner's behalf", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        await repository.setApprovalForAll(accounts[2], true);

        const result = await repository.safeTransferFrom(accounts[0], accounts[1], 1, {from: accounts[2]});

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            log.event.should.equal("Transfer");
        }
    });

    it("sets multiple operators on an owner's tokens", async () => {
        await repository.archive(assetUris[0], orbitdbUris[0]);

        await repository.setApprovalForAll(accounts[2], true);
        await repository.setApprovalForAll(accounts[3], true);

        var isApproved = await repository.isApprovedForAll(accounts[0], accounts[2]);

        isApproved.should.be.true;

        isApproved = await repository.isApprovedForAll(accounts[0], accounts[3]);

        isApproved.should.be.true;

        isApproved = await repository.isApprovedForAll(accounts[0], accounts[4]);

        isApproved.should.be.false;
    });

    it("gets an asset", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0]);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = Number(log.args.tokenId);

                var asset = await repository.getAsset(tokenId);

                asset.should.equal(assetUris[0]);

                break;
            }
        }
    });
});
