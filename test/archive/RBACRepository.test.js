import { getBytes32FromMultihash, getMultihashFromBytes32, getMultihashFromContractResponse } from '../helpers/multihash';
import expectEvent from "zeppelin-solidity/test/helpers/expectEvent";
import assertRequire from '../helpers/assertRequire';
import assertRevert from "zeppelin-solidity/test/helpers/assertRevert";

var expect = require('chai').expect;
var should = require('chai').should();

var RBACRepository = artifacts.require("./archive/RBACRepository.sol");

contract('RBACRepository', function(accounts) {
    let repository;

    const orbitdbUris = [
        '/orbitdb/QmWuXFFKkTA477sb7DRRojZm83YvoWYG5hWNVVabrA7LXS/metadata'
    ];

    const ipfsHashes = [
        'QmahqCsAUAw7zMv6P6Ae8PjCTck7taQA6FgGQLnWdKG7U8',
        'Qmb4atcgbbN5v4CDJ8nz5QG5L2pgwSTLd3raDrnyhLjnUH',
    ];

    function getIPFSHash() {
        return getBytes32FromMultihash(ipfsHashes[0]);
    }

    beforeEach(async () => {
        repository = await RBACRepository.new("KnowledgeArc.Network", "KNOWLEDGEARC");
    });
});
