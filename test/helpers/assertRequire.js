export default async (promise) => {
    try {
        await promise;
        assert.fail('Expected require not received');
    } catch (error) {
        const requireFound = error.message.search('invalid opcode') >= 0;
        assert(requireFound, `Expected "require", got ${error} instead`);
    }
};
