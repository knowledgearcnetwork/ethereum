import { getBytes32FromMultihash, getMultihashFromContractResponse } from '../../helpers/multihash';
import expectEvent from "zeppelin-solidity/test/helpers/expectEvent";
import assertRequire from '../../helpers/assertRequire';

var expect = require('chai').expect;
var should = require('chai').should();

var TimedEmbargo = artifacts.require("./rules/embargo/TimedEmbargo.sol");
var RestrictedRepository = artifacts.require("./archive/RestrictedRepository.sol");

contract('TimedEmbargo', function(accounts) {
    let timedEmbargo;
    let repository;

    let now;

    const orbitdbUris = [
        '/orbitdb/QmWuXFFKkTA477sb7DRRojZm83YvoWYG5hWNVVabrA7LXS/metadata'
    ];

    const assetUris = [
        'ipfs://Qmb4atcgbbN5v4CDJ8nz5QG5L2pgwSTLd3raDrnyhLjnUH',
        'http://www.example.com/asset.pdf',
        'ftp://ftp.example.com/asset.ogv',
    ];

    beforeEach(async () => {
        now = Math.floor(Date.now() / 1000);

        repository = await RestrictedRepository.new("KnowledgeArc.Network", "KNOWLEDGEARC");
        timedEmbargo = await TimedEmbargo.new(repository.address);
    });

    it("embargoes an item for zero time", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0], timedEmbargo.address);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = log.args.tokenId;

                await timedEmbargo.setEmbargoedUntil(tokenId, now);

                assert.isTrue(await timedEmbargo.validate(tokenId));

                break;
            }
        }
    });

    it("embargoes an item sometime into the future", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0], timedEmbargo.address);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = log.args.tokenId;

                await timedEmbargo.setEmbargoedUntil(tokenId, now+120000);

                var validate = await timedEmbargo.validate(tokenId);
                validate.should.be.false;

                var isAccessible = await repository.isAccessible(tokenId);
                isAccessible.should.be.false;

                break;
            }
        }
    });

    it("determines if an embargoed item has a rule attached", async () => {
        var tokenId = 1;

        await timedEmbargo.setEmbargoedUntil(tokenId, now+120000);

        var validate = await timedEmbargo.validate(tokenId);
        validate.should.be.false;

        var isAccessible = await repository.isAccessible(tokenId);
        isAccessible.should.be.true;
    });

    it("gets a rule for the repository asset token", async () => {
        var result = await repository.archive(assetUris[0], orbitdbUris[0], timedEmbargo.address);

        for (var i = 0; i < result.logs.length; i++) {
            var log = result.logs[i];

            if (log.event == "AssetArchived") {
                var tokenId = log.args.tokenId;

                await timedEmbargo.setEmbargoedUntil(tokenId, now+120000);

                var rule = await repository.getRule(tokenId);

                rule.should.equal(timedEmbargo.address);

                break;
            }
        }
    });
});
