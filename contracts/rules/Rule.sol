pragma solidity ^0.4.24;

contract Rule {
    function validate(uint token) public view returns (bool);
}
