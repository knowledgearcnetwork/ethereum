pragma solidity ^0.4.24;

import '../Rule.sol';

contract TimedEmbargo is Rule {
    mapping(uint => uint) internal tokenToUntil;

    address repository;

    constructor(address _repository) public {
        repository = _repository;
    }

    function setEmbargoedUntil(uint token, uint until) public {
        tokenToUntil[token] = until;
    }

    function getEmbargoedUntil(uint token) public view returns (uint) {
        return tokenToUntil[token];
    }

    function validate(uint token) public view returns (bool) {
        uint until = getEmbargoedUntil(token);

        if (until > block.timestamp) {
            return false;
        }

        return true;
    }
}
