pragma solidity ^0.4.24;

import "zeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";

/**
 * @title Repository of archived assets
 * @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
 */
contract Repository is ERC721Token {
    event AssetArchived(address owner, uint tokenId);

    // A list of asset "tokens".
    string[] public assets;

    uint tokenCount;

    // Mapping from a token to an asset's position in the asset array.
    mapping(uint => uint) internal tokenToAssetIndex;

    /**
     * @dev Instantiate the repository.
     * @param _name the name of the repository
     * @param _symbol a short code for the repository
     */
    constructor(string _name, string _symbol)
        ERC721Token(_name, _symbol)
        public
    {
        tokenCount = 0;
    }

    /**
     * @dev Archives an asset and its metadata to the repository.
     * @param _assetUri The uri to the asset
     * @param _metadataUri The uri to the metadata
     * @return string the newly minted token
     */
    function archive(string _assetUri, string _metadataUri)
        public
        returns (uint tokenId)
    {
        uint index = assets.push(_assetUri) - 1;
        tokenId = index + 1;

        tokenToAssetIndex[tokenId] = index;

        _mint(msg.sender, tokenId);
        _setTokenURI(tokenId, _metadataUri);

        emit AssetArchived(msg.sender, tokenId);
    }

    /**
     * @dev Gets an asset from the repository using the token id.
     * @param _tokenId id of the asset to retrieve
     * @return the assetUri
     */
    function getAsset(uint _tokenId) public view returns (string) {
        uint index = tokenToAssetIndex[_tokenId];
        return assets[index];
    }
}
