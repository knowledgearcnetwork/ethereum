pragma solidity ^0.4.24;

import "zeppelin-solidity/contracts/ownership/rbac/RBAC.sol";
import './Repository.sol';

/**
 * @title A repository with asset-specific access and permission settings.
 */
contract RBACRepository is Repository, RBAC  {

}
