pragma solidity ^0.4.24;

import './Repository.sol';
import '../rules/Rule.sol';

/**
 * @title Repository of archived assets, with access restrictions based on
 * rules set out in external contracts.
 */
contract RestrictedRepository is Repository {
    mapping(uint => address) internal tokenToRuleIndex;

    constructor(string _name, string _symbol)
        Repository(_name, _symbol)
        public
    {

    }

    /**
     * @dev Archives an asset and its metadata to the repository.
     * archive will mint its own token and return it as part of the
     * AssetArchived event.
     * @param _assetUri The asset's uri
     * @param _metadataUri The asset's metadata
     * @param _rule address of the rule to apply in the case that the asset
     * should not be immediately shown
     * @return the newly minted token
     */
    function archive(
        string _assetUri,
        string _metadataUri,
        address _rule)
        public
        returns (uint tokenId)
    {
        tokenId = archive(_assetUri, _metadataUri);

        tokenToRuleIndex[tokenId] = _rule;
    }

    /**
     * @dev Gets a rule stored against a token id.
     * @param _tokenId id of the rule to retrieve
     * @return true if the asset is viewable, false otherwise
     */
    function getRule(uint _tokenId) public view returns (address) {
        return tokenToRuleIndex[_tokenId];
    }

    /**
     * @dev Determines if the asset is viewable. True if it is, false otherwise.
     * @param _tokenId id of the asset to retrieve
     * @return true if the asset is viewable, false otherwise
     */
    function isAccessible(uint _tokenId) public view returns (bool) {
        address ruleAddress = getRule(_tokenId);

        if (ruleAddress == address(0)) {
            return true;
        }

        Rule rule = Rule(ruleAddress);

        return rule.validate(_tokenId);
    }
}
