pragma solidity ^0.4.24;

contract IPFSRepository {
    struct Item {
        string metadata;
        string asset;
    }

    mapping (address => uint256[]) internal ownedItems;

    mapping (uint256 => uint256) internal ownedItemsIndex;

    Item[] internal items;

    function save(string _metadata, string _asset) public {
        Item memory item = Item(_metadata, _asset);

        uint256 _index = items.length;

        ownedItems[msg.sender].push(_index);
        ownedItemsIndex[_index] = _index;
        items.push(item);
    }

    function getMetadata(uint _id) public view returns (string hash) {
        return items[_id].metadata;
    }

    function getAsset(uint _id) public view returns (string hash) {
        return items[_id].asset;
    }
}
